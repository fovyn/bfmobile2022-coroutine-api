package be.bf.demo.http

import be.bf.demo.User
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

sealed interface UserApi {
    @GET("users")
    fun getUsers(): Call<List<User>>

    @POST("users")
    fun create(@Body user: User): Call<User>
}