package be.bf.demo

object Config {
    object Database {
        const val URL = "jdbc:sqlite"
        const val USERNAME = "root"
    }
    object File {
        const val USER_PROPERTIES = "properties.yaml"
    }
}