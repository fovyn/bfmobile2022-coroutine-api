package be.bf.demo.services

import be.bf.demo.User
import be.bf.demo.http.RetrofitClient
import be.bf.demo.http.UserApi
import kotlinx.coroutines.*

class UserService {
    private val retrofit = RetrofitClient.getClient()
    private val userApi = retrofit.create(UserApi::class.java)


    fun getAll(): List<User>? {
        val response = userApi.getUsers().execute();

        if (response.isSuccessful && response.code() == 200) {
            return response.body();
        }
        return null;
    }

    suspend fun create(user: User): User? = coroutineScope {
        val asyncResponse = async { userApi.create(user).execute() }
        val response = asyncResponse.await();
        if (response.isSuccessful && response.code() == 201) {
            println("Created ${response.body()}")
            return@coroutineScope response.body()
        }
        return@coroutineScope null;
    }
}