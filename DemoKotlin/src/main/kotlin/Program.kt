import be.bf.demo.User
import be.bf.demo.services.UserService
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking

fun main(): Unit = runBlocking {
    val userService = UserService()

//    val user = User()
//    val created = async { userService.create(user) }
//    created.invokeOnCompletion { println("BLOP") }

    val users = userService.getAll()

    users?.onEach {
        println(it)
    }
}